function dydt = partialImunity(t, y, R0, sigma)
a = sigma;
dydt = [-R0*y(1)*y(2); R0*y(1)*y(2)-y(2)+a*R0*y(2)*(1-y(1)-y(2))];