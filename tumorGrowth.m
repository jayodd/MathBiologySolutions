function dy = tumorGrowth(t, y)
b = 4.2;
d = 0.5;
delta = 0.1;

dy = [0 0]';
n = y(1);
m = y(2);
dy(1) = n*(1-n-m)-d*n;
dy(2) = b*m*(1-n-m)-(d+delta)*m;