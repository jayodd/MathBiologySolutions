function prod = mydotprod(x, y)
if length(x) == length(y)
    for i = 1:length(x)
        temp(i) = x(i)*y(i)
    end
    prod = sum(temp);
else
    error('Input vectors different lengths. Try again.')
end
