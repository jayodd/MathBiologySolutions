%% tutorialOneSoln.m
% Jay A Mackenzie
% j.mackenzie.3@research.gla.ac.uk
%% Q1: Create a row vector containing the numbers 1, 7 and -3.
[1, 7, -3]

%% Q2
A = [0 1 -1 1 2 -1 -1 1 0]

%% Q3
a = 1:11

%% Q4
% The command x = [1:1:5] creates a row vector whose elements are the
% integers between 1 and 5 in increments of 1, i.e. 1, 2, 3, 4, 5

y = 0.1:0.25:2.6

%% Q5
who
% currently defined variables are printed to the command window

%% Q6

b = [0:0.1:1]'

% the ' is the transpose in MatLab

%% Q7

help sum

% to sum rows of A:
sumRowA = sum(A')

% to sum columns of A:
sumColA = sum(A)

%% Q8

ab = a*b
absize = size(ab)

ba = b*a
basize = size(ba)

%% Q9

help eye
% eye(n) outputs the n by n identity matrix
eye(4)

%% Q10

help rand

rand(3, 5)*10

%% Q11

C = diag(ones(1, 5), 0) + diag(2*ones(1, 4), 1) - diag(ones(1, 4), -1)

%% Q12
help eig
% finds the eigenvalues of a given matrix
eig(C)


%% Q13
% the function
%function f = myfun1(x)
%    f = exp(-x.^2).*cos(x);
% should be stored in a seperate script myfun1.m
    
myfun1(0)

myfun1(0:10)


%% Q14
prod = mydotprod(a, b)

%% Q15
maxv = mymax(-10:2:5)

%% Q16

help ode45

%% Q17
% the following are given in the question
u(1) = 1.25; % the first time point is at position 1 in the time vector
v(1) = 0.66;
t = 0:0.01:16;

%% Q18
figure;
[T,Y] = ode45(@LotkaVoltera,0:0.01:16,[1.25 0.66]);
plot(T,Y(:,1),'k', T,Y(:,2),'k--')
xlabel('Time')
ylabel('Population density')
legend('u(t), prey','v(t), predator')

%% Q19
% analytic question

%% Q20
figure;
[T,Y] = ode45(@tumorGrowth,0:0.01:50,[0.5 0.5]);
plot(T,Y(:,1),'k', T,Y(:,2),'k--', 'LineWidth', 3)
xlabel('Time')
ylabel('Cell density')
legend('n(t), normal cell','m(t), mutant cells')

%% Q21
d=0.5; delta=0.1; b=4.2;
n = [0:0.01:1.5];
hold on
plot([0 0], [0 1.5], 'r:', 'LineWidth', 3) % n = 0 nullcline
plot(n,1-n-d,'r:', 'LineWidth', 3) % m=1-n-d, n-nullcline
legend('n(t), normal cell','m(t), mutant cells','n = 0 nullcline','m=1-n-d, nullcline')

%% Q22
plot(0,0,'*','MarkerSize',2) % Extinction St st
plot(0,1-(d+delta)/b,'*','MarkerSize',2) % Tumour only st st
plot(1-d,0,'*','MarkerSize',2) %Normal tissue st st