%% mymax.m
% takes a vector v and returns the value that is furthest from 0
function maxv = mymax(v)
absv = abs(v);
maxv = max(absv);