function dy = LotkaVoltera(t, y)
a = 1.095;
dy = [0 0]';
u = y(1);
v = y(2);
dy(1) = u*(1-v);
dy(2) = a*v*(u-1);