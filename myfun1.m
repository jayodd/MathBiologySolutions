function f = myfun1(x)
    f = exp(-x.^2).*cos(x);