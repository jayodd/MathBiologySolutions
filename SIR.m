function dydt = SIR(t, y, R0)
dydt = [-R0*y(1)*y(2); R0*y(1)*y(2)-y(2)];