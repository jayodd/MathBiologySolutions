function dydt = odefcn(t,y,A,B)
dydt = [y(2); (A/B)*t.*y(1)];