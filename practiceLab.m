%% practiceLab.m
% J.A.M.s solution to the practice assesment for Mathematical Biology at
% the University of Glasgow.
% Winter 2021.
close all
clear all
clc
%% Q1:

R0 = 3.5; % given basic reproductive rate
tspan = [0 10]; % temporal span from question
y0 = [0.99; 0.01]; % initial conditions [S(0), I(0)]
[t,y] = ode45(@(t,y) SIR(t,y,R0), tspan, y0); % call ode45 to solve the equations

plot(t,y(:,1),'-o',t,y(:,2),'-.', 'LineWidth', 3) % plot the solutions for S, I
hold on
plot(t, ones(size(y(:, 1)))-y(:, 1)-y(:, 2), ':', 'LineWidth', 3) % Find and plot R
legend('Susceptible, S', 'Infected, I', 'Recovered, R')
ylabel('Proportion of population')
xlabel('Time, days')

%%
figure;
for R0 = 0:5:50
    tspan = [0 10];
    y0 = [0.99; 0.01];
    [t,y] = ode45(@(t,y) SIR(t,y,R0), tspan, y0); % call ode45 to solve the equations
    plot(y(:, 1), y(:, 2), ':', 'LineWidth', 3) % Find and plot R
    hold on
end


%% partial immunity

R0 = 5; % given basic reproductive rate
tspan = [0 10]; % temporal span from question
y0 = [0.99; 0.01]; % initial conditions [S(0), I(0)]
ind = 1;

for sigma = 0:10
[t,y] = ode45(@(t,y) partialImunity(t,y,R0, sigma), tspan, y0); % call ode45 to solve the equations

%colour = rand(3, 1);
hold on

figure(3);

subplot(3, 1, 1); hold on
plot(t,y(:,1),'LineWidth', 3)
%,'Color', colour) % plot the solutions for S

subplot(3, 1, 2); hold on
plot(t,y(:,2), 'LineWidth', 3)%,'Color', colour) % plot the solutions for I

subplot(3, 1, 3); hold on
plot(t, ones(size(y(:, 1)))-y(:, 1)-y(:, 2), 'LineWidth', 3)
%legend('Susceptible, S', 'Infected, I', 'Recovered, R')
%ylabel('Proportion of population')

legend
end
xlabel('Time, days')


