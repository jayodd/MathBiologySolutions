%% TutorialTwoSoln.m

%% Q3
p = 1; x = p;
for j = 1:10
    p = 1.3*p;
    x = [x; p];
end
x

% The initial condition is p = 1 which is stored in the first row of the
% vector x. The second value for p is computed for the first index that we
% loop over and store this in the second row of x. We update the value of p
% in every loop, so the current step is a function of the last. Do this 9
% additional times.

%% Q4
p(1)=1
for j=1:10
p(j+1)=1.3*p(j)
end
size(p)
%  p has length(11), as there are 10 itterations in the loop in addition to
%  the initial condition.
%  we cannot start with p(0) as matlab indexing starts from 1, not 0; we
%  may only index over the stricltly positive integers.
%% Q5
plot(0:10, p, '*')
%% Q6
%  I didn't find the analytical solution, so haven't done this question
%% Q7
clear r t N
r = [0.2 0.8 1 2];
%figure; hold on
t = 4;
N(1, 1:4) = 1;
for j = 1:4
    for i = 1:t
        N(i+1, j) = N(i, j)*r(j);
    end
end
hold on
plot(1:(t+1), N)

%% Q8
clear r t N
r = 1.3;
t = 10;
N(1, 1:t) = 1;
for j = 1:1
    for i = 1:t
        N(i+1, j) = N(i, j)*r(j);
    end
end
hold on
%plot(1:(t+1), N)
plot(N(1:t-1, 1), N(2:t, 1))
%% Q8
p(1)=1;
maxVal = [10 100 1000];
for i = 1:length(maxVal)
for j=1:1000
    if p(j) > maxVal(i)
        [p(j), j]
        break
    end
    p(j+1)=1.3*p(j);
end
end

%% Q10
r = 1.5;
K = 100;
N(1) = 5; %N_0 = 5
t = 0:10;
for i = 1:length(t)-1
    N(i+1) = N(i)*exp(r*(1-(N(i)/K)));
end
plot(t, N)
xlabel('Time'); ylabel('Popn')

%% Q11

hold on
r = 2;
K = 100;
N(1) = 5; %N_0 = 5
t = 0:10;
for i = 1:length(t)-1
    N(i+1) = N(i)*exp(r*(1-(N(i)/K)));
end
plot(t, N)
xlabel('Time'); ylabel('Popn')

r = 0.5;
K = 100;
N(1) = 5; %N_0 = 5
t = 0:10;
for i = 1:length(t)-1
    N(i+1) = N(i)*exp(r*(1-(N(i)/K)));
end
plot(t, N)
xlabel('Time'); ylabel('Popn')

%% Q12
%help pdepe
clear x mid i x t
x = linspace(-20, 20, 11);
mid = [];
for i = 1:length(x)
    if (x(i) >-1) & (x(i) < 1)
        mid = [mid; i];
    end
end
t = 0:1:50;

u = zeros(length(t), length(x));
u(1, mid) = 1;
%%
clear all
close all
x = linspace(-20, 20, 100);
t = 0:0.5:50;
m = 1;
sol = pdepe(0,@pdffun,@pdfic,@pdfbc,x,t);
u = sol(:,:,1);
mesh(x,t,u)
xlabel('x')
ylabel('t')
zlabel('u(x,t)')
legend('surf')
hold on
plot3(x, 0*ones(size(x)), u(1, :, :), 'r', 'LineWidth', 3)
plot3(x, 10*ones(size(x)), u(21, :, :), 'g', 'LineWidth', 3)
plot3(x, 20*ones(size(x)), u(41, :, :), 'b', 'LineWidth', 3)
plot3(x, 30*ones(size(x)), u(61, :, :), 'c', 'LineWidth', 3)
plot3(x, 40*ones(size(x)), u(81, :, :), 'm', 'LineWidth', 3)
plot3(x, 50*ones(size(x)), u(101, :, :), 'k', 'LineWidth', 3)
legend('surf', 'soln at t = 0', 'soln at t = 10', 'soln at t = 20', ...
    'soln at t = 30', 'soln at t = 40', 'soln at t = 50')

%%
figure;
hold on
plot(x, u(1, :, :), 'r', 'LineWidth', 3)
plot(x, u(21, :, :), 'r', 'LineWidth', 3)
plot(x, u(41, :, :), 'r', 'LineWidth', 3)
plot(x, u(61, :, :), 'r', 'LineWidth', 3)
plot(x, u(81, :, :), 'r', 'LineWidth', 3)
plot(x, u(101, :, :), 'r', 'LineWidth', 3)